# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
unsetopt beep
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/zaz/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

source ~/.zaliases
source /home/zaz/.cargo/env
cat /home/zaz/.cache/wal/sequences
export PATH="${PATH}:/home/zaz/.emacs.d/bin/"
export EDITOR=nvim
export VISUAL=nvim
pfetch
