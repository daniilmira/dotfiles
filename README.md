# Dotfiles

Dotfiles are the customization files (their filenames usually begin with a period) that are used to personalize your linux or other Unix-based system. This repository contains my personal dotfiles.

My config gets random pokemon wallpaper from home/pictures/pokemon folder.

![screenshot](https://i.imgur.com/AHLAhmv.png)
![screenshot](https://i.imgur.com/YopuQ2T.png)

```
 alacritty  > the fastest terminal emulator with GPU acceleration
 bspwm      > tiling window manager
 picom      > pico compositor
 polybar    > status bar
 ranger     > file manager
 rofi       > application launcher
 sxhkd      > keyboard daemon
 xorg       > X.Org server
 zsh        > unix shell
```

# Usage
I use [stow](https://www.gnu.org/software/stow/) to manage my dotfiles
```
git clone https://gitlab.com/daniilmira/dotfiles.git
cd ~/dotfiles
stow polybar # or any program you want to. Also you can use * operator to stow everything. And --adopt to delete your files and replace them with my config.
```
